package com.example.movies;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class MovieServiceTest {

    @InjectMocks
    private MovieService movieService;

    @Mock
    private MovieRepository movieRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testAllMovies() {
        Movie movie1 = new Movie(new ObjectId(), "tt1234567", "Movie 1", "2023-01-01", "link1", "poster1", Arrays.asList("Action"), Arrays.asList("backdrop1"), null);
        Movie movie2 = new Movie(new ObjectId(), "tt7654321", "Movie 2", "2023-01-02", "link2", "poster2", Arrays.asList("Comedy"), Arrays.asList("backdrop2"), null);
        List<Movie> movies = Arrays.asList(movie1, movie2);

        when(movieRepository.findAll()).thenReturn(movies);

        List<Movie> result = movieService.allMovies();

        assertEquals(2, result.size());
        assertEquals("Movie 1", result.get(0).getTitle());
        assertEquals("Movie 2", result.get(1).getTitle());
    }

    @Test
    public void testSingleMovie() {
        String imdbId = "tt1234567";
        Movie movie = new Movie(new ObjectId(), imdbId, "Movie 1", "2023-01-01", "link1", "poster1", Arrays.asList("Action"), Arrays.asList("backdrop1"), null);

        when(movieRepository.findMovieByImdbId(imdbId)).thenReturn(Optional.of(movie));

        Optional<Movie> result = movieService.singleMovie(imdbId);

        assertTrue(result.isPresent());
        assertEquals("Movie 1", result.get().getTitle());
    }
}
