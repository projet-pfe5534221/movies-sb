package com.example.movies;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@WebMvcTest(MovieController.class)
public class MovieControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MovieService movieService;

    @InjectMocks
    private MovieController movieController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(movieController).build();
    }

    @Test
    public void testGetAllMovies() throws Exception {
        Movie movie1 = new Movie(new ObjectId(), "tt1234567", "Movie 1", "2023-01-01", "link1", "poster1", Arrays.asList("Action"), Arrays.asList("backdrop1"), null);
        Movie movie2 = new Movie(new ObjectId(), "tt7654321", "Movie 2", "2023-01-02", "link2", "poster2", Arrays.asList("Comedy"), Arrays.asList("backdrop2"), null);
        List<Movie> movies = Arrays.asList(movie1, movie2);

        when(movieService.allMovies()).thenReturn(movies);

        mockMvc.perform(get("/api/v1/movies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$[0].imdbId").value("tt1234567"))
                .andExpect(jsonPath("$[0].title").value("Movie 1"))
                .andExpect(jsonPath("$[1].imdbId").value("tt7654321"))
                .andExpect(jsonPath("$[1].title").value("Movie 2"));
    }

    @Test
    public void testGetSingleMovie() throws Exception {
        String imdbId = "tt1234567";
        Movie movie = new Movie(new ObjectId(), imdbId, "Movie 1", "2023-01-01", "link1", "poster1", Arrays.asList("Action"), Arrays.asList("backdrop1"), null);

        when(movieService.singleMovie(imdbId)).thenReturn(Optional.of(movie));

        mockMvc.perform(get("/api/v1/movies/{imdbId}", imdbId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.imdbId").value(imdbId))
                .andExpect(jsonPath("$.title").value("Movie 1"));
    }
}
