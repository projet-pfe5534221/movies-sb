package com.example.movies;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MovieApplicationTests {
    @Test
    void contextLoads() {
        // This test will fail if the application context cannot start
    }
}
