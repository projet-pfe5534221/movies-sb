package com.example.movies;

import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.eq;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class ReviewControllerTest {

    @InjectMocks
    private ReviewController reviewController;

    @Mock
    private ReviewService reviewService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreateReview() {
        Map<String, String> payload = new HashMap<>();
        payload.put("body", "Great movie!");
        payload.put("imdbId", "tt1234567");

        Review review = new Review("Great movie!");

        when(reviewService.createReview(eq("Great movie!"), eq("tt1234567"))).thenReturn(review);

        ResponseEntity<Review> response = reviewController.createReview(payload);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals("Great movie!", response.getBody().getBody());
    }
}
